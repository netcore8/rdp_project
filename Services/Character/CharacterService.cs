using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RPG_Project.Data;
using RPG_Project.DTOs;
using RPG_Project.Models;

namespace RPG_Project.Services.Character
{
    public class CharacterService : ICharacterService
    {
        private readonly AppDBContext _dBContext;
        private readonly IMapper _mapper;
        private readonly ILogger _log;

        public CharacterService(AppDBContext dBContext, IMapper mapper, ILogger<CharacterService> log)
        {
            this._dBContext = dBContext;
            this._mapper = mapper;
            this._log = log;
        }

        public async Task<ServiceResponse<GetCharacterDto>> AddCharacterSkill(AddCharacterSkillDto newSkill)
        {
            var character = await _dBContext.Characters.Include(x => x.Weapon).Include(x => x.CharacterSkill).ThenInclude(x => x.Skill).FirstOrDefaultAsync(x => x.Id == newSkill.CharacterId);

            if (character == null)
            {
                return ResponseResult.Failure<GetCharacterDto>("Character not found");
            }

            var skill = await _dBContext.Skills.FirstOrDefaultAsync(x => x.Id == newSkill.SkillId);

            if (skill == null)
            {
                return ResponseResult.Failure<GetCharacterDto>("Skill not found");
            }

            var c_Skill = new CharacterSkill();

            c_Skill.CharacterId = newSkill.CharacterId;
            c_Skill.SkillId = newSkill.SkillId;

            _dBContext.CharacterSkills.Add(c_Skill);
            await _dBContext.SaveChangesAsync();

            var dto = _mapper.Map<GetCharacterDto>(character);
            return ResponseResult.Success(dto);

        }

        public async Task<ServiceResponse<GetCharacterDto>> AddWeapon(AddWeaponDto newWeapon)
        {
            var character = await _dBContext.Characters.Include(x => x.Weapon).FirstOrDefaultAsync(x => x.Id == newWeapon.CharacterId);

            if (character == null)
            {
                return ResponseResult.Failure<GetCharacterDto>("Character not found");
            }

            var weapon = new Weapon
            {
                Name = newWeapon.Name,
                Damage = newWeapon.Damage,
                CharacterId = newWeapon.CharacterId
            };

            _dBContext.Weapons.Add(weapon);
            await _dBContext.SaveChangesAsync();

            var dto = _mapper.Map<GetCharacterDto>(character);
            return ResponseResult.Success(dto);
        }

        public async Task<ServiceResponse<List<GetCharacterDto>>> GetAllCharacters()
        {
            var characters = await _dBContext.Characters.Include(x => x.Weapon).AsNoTracking().ToListAsync();
            var dto = _mapper.Map<List<GetCharacterDto>>(characters);

            return ResponseResult.Success(dto);
        }

        public async Task<ServiceResponse<GetCharacterDto>> GetCharacterById(int characterId)
        {
            var character = await _dBContext.Characters.FirstOrDefaultAsync(x => x.Id == characterId);

            if (character == null)
            {
                return ResponseResult.Failure<GetCharacterDto>("Character not found.");
            }

            var dto = _mapper.Map<GetCharacterDto>(character);

            return ResponseResult.Success(dto);
        }
    }
}